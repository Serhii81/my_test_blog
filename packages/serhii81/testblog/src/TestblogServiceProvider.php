<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


namespace Serhii81\Testblog;
use Illuminate\Support\ServiceProvider;

class TestblogServiceProvider extends ServiceProvider 
{
    public function boot(){
        include __DIR__.'/Http/routes.php';

        $this->loadViewsFrom(__DIR__.'/Views', 'testblog');
    }
    
    public function register(){
        
    }
}